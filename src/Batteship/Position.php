<?php

namespace Battleship;

use Exception;

class Position
{
    /**
     * @var string
     */
    private $column;
    private $row;

    /**
     * Position constructor.
     * @param string $letter
     * @param string $number
     */
    public function __construct($letter, $number)
    {
        if (empty($letter) || strlen($number) == 0) {
            throw new Exception("Input argument(s) are empty");
        }

        $this->column = Letter::validate(strtoupper($letter));

        if(!is_numeric($number)) {
            throw new Exception("Second argument is not a number: $number");
        }

        $this->row = $number;
    }

    public function getColumn()
    {
        return $this->column;
    }

    public function getRow()
    {
        return $this->row;
    }

    public function __toString()
    {
        return sprintf("%s%s", $this->column, $this->row+1);
    }

    public function isEqual($other) {
        return $this->__toString() === $other->__toString();
    }
}
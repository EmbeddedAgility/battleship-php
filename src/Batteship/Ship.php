<?php

namespace Battleship;

use InvalidArgumentException;

class Ship
{

    private $name;
    private $size;
    private $color;
    private $positions = array();
    private $hitPositions = array();
    private $sunk = false;

    public function __construct($name, $size, $color = null)
    {
        $this->name = $name;
        $this->size = $size;
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    public function resetPositions() {
        $this->positions = array();
    }

    public function positionValid($input, $fleet, $boardSize) {
        list($letter, $number) = $this->splitPosition($input);
        if ($number > $boardSize[1] || $number < 1) {
            throw new InvalidArgumentException("Invalid Number");
        }

        if(Letter::key($letter) >= $boardSize[0]) {
            throw new InvalidArgumentException("Letter is not included in this board size");
        }

        $position = new Position($letter, $number-1);

        if ($this->isCollision($position, $fleet)) {
            throw new InvalidArgumentException("Invalid Position");
        }
    }

    public function addPosition($input)
    {
        list($letter, $number) = $this->splitPosition($input);
        array_push($this->positions, new Position($letter, $number-1));
    }

    public function &getPositions()
    {
        return $this->positions;
    }

    public function &getHitPositions()
    {
        return $this->hitPositions;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function addHitPosition($position){
        array_push($this->hitPositions, $position);

        if(count($this->hitPositions) == count($this->positions)) {
            $this->sunk = true;
        }
    }

    public function isSunk() {
        return $this->sunk;
    }

    private function splitPosition($input) {
        $letter = substr($input, 0, 1);
        $number = substr($input, 1, strlen($input) - 1);

        return array($letter, $number);
    }

    // private function validFirstPosition($letter, $number, $fleet) {
    //     if ($this->hasCollision($letter, $number, $fleet)) {
    //         throw new InvalidArgumentException("invalid position");
    //     }

    //     if ($this->validatePositive($number)) {
    //         return true;
    //     }
    //     if ($this->validateNegative($number)) {
    //        return true;
    //     }
    //     if ($this->validateNegative(Letter::key($letter))) {
    //         return true;
    //     }
    //     if ($this->validatePositive(Letter::key($letter))) {
    //         return true;
    //     }
        
    //     throw new InvalidArgumentException("invalid position");
    // }

    public function validatePositive($number, $size) {
        if (($number + $this->size) > $size) {
            return false;
        }
        return true;
    }

    public function validateNegative($number) {
        if (($number - $this->size) < 1) {
            return false;
        }
        return true;
    }

    public function isCollision($position, $fleet) {
        foreach ($fleet as $ship) {
            $positions = $ship->getPositions();
            foreach ($positions as $p) {
                if ($position->isEqual($p)) {
                    return true;
                }
            }
        }
        return false;
    }
}
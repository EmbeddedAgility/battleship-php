<?php

namespace Battleship;

use InvalidArgumentException;
use Exception;

class GameController
{

    public static function checkIsHit(array &$fleet, $shot)
    {
        if ($fleet == null) {
            throw new InvalidArgumentException("ships is null");
        }

        if ($shot == null) {
            throw new InvalidArgumentException("shot is null");
        }

        foreach ($fleet as $ship) {
            foreach ($ship->getPositions() as $position) {
                if ($position == $shot) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function registerHit(array &$fleet, $shot) {
        foreach ($fleet as $ship) {
            foreach ($ship->getPositions() as $position) {
                if ($position == $shot) {
                    $ship->addHitPosition($position);
                    return;
                }
            }
        }
    }

    public static function checkIsFleetSunk(array $fleet){
        if ($fleet == null) {
            throw new InvalidArgumentException("ships is null");
        }

        foreach ($fleet as $ship) {
            if (!$ship->isSunk()) {
                return false;
            }
        }

        return true;
    }

    public static function initializeShips()
    {
        return Array(
            new Ship("Aircraft Carrier", 5, Color::CADET_BLUE),
            new Ship("Battleship", 4, Color::RED),
            new Ship("Submarine", 3, Color::CHARTREUSE),
            new Ship("Destroyer", 3, Color::YELLOW),
            new Ship("Patrol Boat", 2, Color::ORANGE));
    }

    public static function isShipValid($ship)
    {
        return count($ship->getPositions()) == $ship->getSize();
    }

    public static function getRandomPosition($boardSize)
    {
        $rows = $boardSize[1];
        $lines = $boardSize[0];

        $letter = Letter::value(random_int(0, $lines - 1));
        $number = random_int(0, $rows - 1);

        return new Position($letter, $number);
    }

    public static function positionRandomly($ships, $boardSize) {
        foreach ($ships as $ship) {
            $validPosition = false;
            while (!$validPosition) {
                $ship->resetPositions();
                $initialPos = self::getRandomPosition($boardSize);
                $direction = rand(0, 3);
                
                $insideBounds = false;
                if ($direction % 2) {
                    $insideBounds = $ship->validatePositive($initialPos->getRow(), $boardSize[1]);
                } else {
                    $insideBounds = $ship->validateNegative($initialPos->getRow());
                }

                if (!$insideBounds) {
                    continue;
                }
        
                $currPosition = $initialPos;
                for ($i = 0; $i < $ship->getSize(); $i++) {
                    try {
                        if ($ship->isCollision($currPosition, $ships)) {
                            continue 2;
                        };
                        $ship->addPosition($currPosition->__toString());
                        $currPosition = self::nextPosition($currPosition, $direction, $boardSize[0]);
                    } catch (Exception $e) {
                        continue 2;
                    }
                }
                $validPosition = true;
            }
        }
        return $ships;
    }

    private static function nextPosition($curr, $dir, $size) {
        switch ($dir) {
            case 0:
                return new Position($curr->getColumn(), $curr->getRow() - 1);
            case 1:
                return new Position($curr->getColumn(), $curr->getRow() + 1);
            case 2:        
                return new Position(Letter::validate(Letter::$letters[Letter::key($curr->getColumn()) - 1], $size), $curr->getRow());
            case 3:
                return new Position(Letter::validate(Letter::$letters[Letter::key($curr->getColumn()) + 1], $size), $curr->getRow());
        }
    }
}
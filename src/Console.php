<?php

use Battleship\Color;

class Console
{
    function resetForegroundColor()
    {
        echo(Battleship\Color::DEFAULT_GREY);
    }

    function setForegroundColor($color)
    {
        echo($color);
    }

    function println($line = "")
    {
        echo "$line\n";
    }
    function print($text = "")
    {
        echo "$text";
    }

    function printAnimationLost() {
        $this->setForegroundColor(Color::BRIGHT_RED);
        $ship = array(
        "                                     |__",
        "                                     |\\/",
        "                                     ---",
        "                                     / | [",
        "                              !      | |||",
        "                            _/|     _/|-++'",
        "                        +  +--|    |--|--|_ |-",
        "                     { /|__|  |/\\__|  |--- |||__/",
        "                    +---------------___[}-_===_.'____                 /\\",
        "                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _",
        " __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7",
        "|                                   R. I. P.                                  BB-61/",
        " \\_________________________________________________________________________|",
        );
        
        $max = 14;
        $total = count($ship);
        while ($total >= 0) {
            $diff = $max - $total;
            while ($diff > 0) {
                $this->println();
                $diff--;
            }

            for ($k = 0; $k < $total; $k++) {
                $this->println($ship[$k]);
            }
            $this->setForegroundColor(Color::BRIGHT_BLUE);
            echo "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\\";
            $this->setForegroundColor(Color::BRIGHT_RED);

            if ($total == $max-1) {
                sleep(1);
            }
            $total--;
            if ($total >= 0) {
                usleep(200000);
                for ($j = 1; $j <= $max; $j++) {
                    echo "\r\033[2K\033[1A";
                }
            }
        }
        echo "\n";

        $this->resetForegroundColor();
    }

    function printAnimationWon() {
        $this->setForegroundColor(Color::GREEN);
        
        $maxSpaces = 40;
        $spaces = 0;
        while ($spaces <= $maxSpaces) {
            $this->println(str_repeat(" ", $spaces) . "                                     |__");
            $this->println(str_repeat(" ", $spaces) . "                                     |\\/");
            $this->println(str_repeat(" ", $spaces) . "                                     ---");
            $this->println(str_repeat(" ", $spaces) . "                                     / | [");
            $this->println(str_repeat(" ", $spaces) . "                              !      | |||");
            $this->println(str_repeat(" ", $spaces) . "                            _/|     _/|-++'");
            $this->println(str_repeat(" ", $spaces) . "                        +  +--|    |--|--|_ |-");
            $this->println(str_repeat(" ", $spaces) . "                     { /|__|  |/\\__|  |--- |||__/");
            $this->println(str_repeat(" ", $spaces) . "                    +---------------___[}-_===_.'____                 /\\");
            $this->println(str_repeat(" ", $spaces) . "                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
            $this->println(str_repeat(" ", $spaces) . " __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
            $this->println(str_repeat(" ", $spaces) . "|                            YOU ARE THE WINNER                            BB-61/");
            $this->println(str_repeat(" ", $spaces) . " \\_________________________________________________________________________|");
            $this->setForegroundColor(Color::BRIGHT_BLUE);
            echo "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\\";
            $this->setForegroundColor(Color::GREEN);

            if ($spaces >= 0 && $spaces != $maxSpaces) {
                usleep(100000);
                for ($j = 1; $j <= 13; $j++) {
                    echo "\r\033[2K\033[1A";
                }
            }
            $spaces++;
        }
        echo "\n";

        $this->resetForegroundColor();
    }
}
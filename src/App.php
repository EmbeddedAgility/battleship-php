<?php

use Battleship\GameController;
use Battleship\Position;
use Battleship\Letter;
use Battleship\Color;

class App
{
    CONST MIN_BOARD_SIZE = 5;
    CONST MAX_BOARD_SIZE = 26;
    private static $myFleet = array();
    private static $enemyFleet = array();
    private static $availableShots = array();
    private static $myHitShots = array();
    private static $enemyHitShots = array();
    private static $boardSize = array();
    private static $console;

    static function run()
    {
        self::$console = new Console();
        self::$console->setForegroundColor(Color::MAGENTA);

        self::$console->println("                                     |__");
        self::$console->println("                                     |\\/");
        self::$console->println("                                     ---");
        self::$console->println("                                     / | [");
        self::$console->println("                              !      | |||");
        self::$console->println("                            _/|     _/|-++'");
        self::$console->println("                        +  +--|    |--|--|_ |-");
        self::$console->println("                     { /|__|  |/\\__|  |--- |||__/");
        self::$console->println("                    +---------------___[}-_===_.'____                 /\\");
        self::$console->println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        self::$console->println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        self::$console->println("|                        Welcome to Battleship                         BB-61/");
        self::$console->println(" \\_________________________________________________________________________|");
        self::$console->println();
        self::$console->resetForegroundColor();
        self::InitializeGame();
        self::StartGame();
    }

    public static function initializeAvailableEnemyShoots() {
        self::$availableShots = array();
        for ($i = 0; $i < self::$boardSize[0]; $i++) {
            for($j = 0; $j < self::$boardSize[1]; $j++) {
                self::$availableShots[] = new Position(Letter::value($i),$j);
            }
        }
        shuffle(self::$availableShots);
        // var_dump(self::$availableShots);
    }
    
    public static function InitializeEnemyFleet()
    {
        self::$enemyFleet = GameController::initializeShips();
        self::$enemyFleet = GameController::positionRandomly(self::$enemyFleet, self::$boardSize);
        foreach (self::$enemyFleet as $ship) {
            self::$console->print($ship->getName() . " -- ");
            foreach ($ship->getPositions() as $pos) {
                self::$console->print($pos , " ");
            }
            self::$console->println("");
        }
    }

    public static function getRandomPosition()
    {
        $lines = self::$boardSize[0];;
        $rows = self::$boardSize[1];

        $letter = Letter::value(random_int(0, $lines - 1));
        $number = random_int(0, $rows - 1);

        return new Position($letter, $number);
    }

    public static function getAvailableRandomPosition()
    {
        return array_pop(self::$availableShots);
    }

    public static function InitializeMyFleet()
    {
        self::$myFleet = GameController::initializeShips();

        self::$console->println("Please position your fleet (Game board has size from A to " . Letter::value(self::$boardSize[0]-1) . " and 1 to " . self::$boardSize[1] . ") :");

        foreach (self::$myFleet as $ship) {

            self::$console->println();
            printf("Please enter the positions for the %s (size: %s)", $ship->getName(), $ship->getSize());

            for ($i = 1; $i <= $ship->getSize(); $i++) {
                printf("\nEnter position %s of %s (i.e A3):", $i, $ship->getSize());
                $input = readline("");
                try {
                    $ship->positionValid($input, self::$myFleet, self::$boardSize);
                    $ship->addPosition($input);
                } catch (InvalidArgumentException $e) {
                    self::$console->println($e->getMessage());
                    $i--;
                } catch (Exception $e) {
                    self::$console->println($e->getMessage());
                    $i--;
                }
                
            }
        }
    }

    public static function beep()
    {
        echo "\007";
    }

    public static function InitializeGame()
    {
        self::InitializeBoard();
        self::initializeAvailableEnemyShoots();
        self::InitializeMyFleet();
        self::InitializeEnemyFleet();
    }

    private static function InitializeBoard() {
        while (true) {
            printf("\nEnter board width(min %s and max %s):", self::MIN_BOARD_SIZE, self::MAX_BOARD_SIZE);
            $inputWidth = readline("");
            if(self::validateBoardSize($inputWidth)){
                break;
            }
            self::$console->println("Wrong size entered.");
        }
        while (true) {
            printf("\nEnter board height(min %s and max %s):", self::MIN_BOARD_SIZE, self::MAX_BOARD_SIZE);
            $inputHeight = readline("");
            if(self::validateBoardSize($inputHeight)){
                break;
            }
            self::$console->println("Wrong size entered.");
        }
        self::$boardSize = [$inputWidth, $inputHeight];
        
    }

    private static function validateBoardSize($size){
        if(is_numeric($size) && $size >= self::MIN_BOARD_SIZE && $size <= self::MAX_BOARD_SIZE) {
            return true;
        }
        return false;
    }

    public static function StartGame()
    {
        self::$console->println("\033[2J\033[;H");
        self::$console->println("                  __");
        self::$console->println("                 /  \\");
        self::$console->println("           .-.  |    |");
        self::$console->println("   *    _.-'  \\  \\__/");
        self::$console->println("    \\.-'       \\");
        self::$console->println("   /          _/");
        self::$console->println("  |      _  /\" \"");
        self::$console->println("  |     /_\'");
        self::$console->println("   \\    \\_/");
        self::$console->println("    \" \"\" \"\" \"\" \"");

        while (true) {
            self::$console->println("");
            self::$console->setForegroundColor(Color::CADET_BLUE);
            self::$console->println("Player, it's your turn");
            self::$console->println("Enter coordinates for your shot :");
            self::$console->resetForegroundColor();

            $position = readline("");

            try {
                $pos = self::parsePosition($position);
                $isHit = GameController::checkIsHit(self::$enemyFleet, $pos);
                echo $isHit ? "Yeah ! Nice hit !" : "Miss";
                self::$console->println();
    	    } catch (InvalidArgumentException $e) {
                echo "Miss\n";
                echo "This position is otside the playing field. Please try again";
                continue;
            } catch (Exception $e) {
                echo $e->getMessage();
                continue;
            }
            
            if ($isHit) {
                GameController::registerHit(self::$enemyFleet, $pos);
                self::$myHitShots[] = $pos;
                self::printHitResults();
            }
            
            self::showFleetMatrix(self::$enemyFleet, self::$myHitShots);
            self::showLeftoverShips(self::$enemyFleet);
            self::$console->println();

            self::enemyFire();
        }
    }

    public static function showFleetMatrix(array $fleet, array $hitShots) {
        $matrix = [];
        self::$console->print(" |");
        for ($col = 0; $col < self::$boardSize[0]; $col++) {
            self::$console->print(Letter::value($col));
        }
        self::$console->print("\n");
        for ($col = 0; $col < self::$boardSize[1]; $col++) {
            $colT = $col+1;
            self::$console->print($colT . "|");
            for($row = 0; $row < self::$boardSize[0]; $row++){
                $position = new Position(Letter::value($row), $col);
                if(in_array($position, $hitShots)) {
                    self::$console->setForegroundColor(Color::RED);
                    self::$console->print("X");
                    self::$console->resetForegroundColor();
                } else {
                    self::$console->setForegroundColor(Color::CADET_BLUE);
                    self::$console->print("-");
                    self::$console->resetForegroundColor();
                }
            }
            self::$console->print("\n");
        }

    }



    public static function showSunkShips(array $fleet){
        $titleShown = false;
        foreach ($fleet as $ship) {
            if($ship->isSunk()) {
                if(!$titleShown) {
                    $titleShown = true;
                    self::$console->println("--------    SUNK SHIPS    --------");
                }

                printf("%s with size of %s\n", $ship->getName(), $ship->getSize());
            }
        }
    }

    public static function showLeftoverShips(array $fleet) {
        self::$console->println("--------  LEFTOVER SHIPS  --------");
        foreach ($fleet as $ship) {
            if(!$ship->isSunk()) {
                printf("%s with size of %s\n", $ship->getName(), $ship->getSize());
            }
        }
    }

    public static function parsePosition($input)
    {
        
        if (strtoupper($input) === "WG") {
            return self::winGame();
        }

        if (strtoupper($input) === "LG") {
            return self::lostGame();
        }

        $letter = substr($input, 0, 1);
        $number = substr($input, 1, 1);

        if (empty($letter) || empty($number)) {
            throw new Exception("Input argument(s) are empty");
        }

        if(!is_numeric($number)) {
            throw new Exception("Second argument is not a number: $number");
        }

        if(Letter::key($letter) >= self::$boardSize[0]) {
            throw new InvalidArgumentException("Letter is not included in this board size");
        }

        if($number < 1 || $number > self::$boardSize[1]) {
            throw new InvalidArgumentException("Number must be between 1 and " . self::$boardSize[1]);
        }

        return new Position($letter, $number-1);
    }

    private static function winGame() {
        $enemyFleet = self::$enemyFleet;
        foreach (self::$enemyFleet as $key => $ship) {
            for ($i=0; $i < count($ship->getPositions()) ; $i++) {
                GameController::registerHit(self::$enemyFleet, $ship->getPositions()[$i]);
                self::$myHitShots[] = $ship->getPositions()[$i];
                self::showFleetMatrix(self::$enemyFleet, self::$myHitShots);
                echo "Yeah ! Nice hit for " . $ship->getPositions()[$i] . "!";
                self::$console->println();
                self::printHitResults();
                self::showLeftoverShips(self::$enemyFleet);
                self::$console->println();
            }
        }
    }

    private static function lostGame() {
        while (true) {
            self::enemyFire();
        }
    }

    private static function printHitResults() {
        self::beep();
        self::$console->println("                \\         .  ./");
        self::$console->println("              \\      .:\" \";'.:..\" \"   /");
        self::$console->println("                  (M^^.^~~:.'\" \").");
        self::$console->println("            -   (/  .    . . \\ \\)  -");
        self::$console->println("               ((| :. ~ ^  :. .|))");
        self::$console->println("            -   (\\- |  \\ /  |  /)  -");
        self::$console->println("                 -\\  \\     /  /-");
        self::$console->println("                   \\  \\   /  /");

        self::showSunkShips(self::$enemyFleet);

        if(GameController::checkIsFleetSunk(self::$enemyFleet)) {
            self::$console->setForegroundColor(Color::ORANGE);
            self::$console->println("You are the winner!"); 
            self::$console->printAnimationWon();
            exit();
        }
    }

    private static function enemyFire() {
        $position= self::getAvailableRandomPosition();
            
        $isHit = GameController::checkIsHit(self::$myFleet, $position);
        self::$console->println();
        printf("Computer shoot in %s%s and %s", $position->getColumn(), $position->getRow()+1, $isHit ? "hit your ship !\n" : "miss\n");
        if ($isHit) {
            self::beep();
            GameController::registerHit(self::$myFleet, $position);
            self::$enemyHitShots[] = $position;

            self::$console->println("                \\         .  ./");
            self::$console->println("              \\      .:\" \";'.:..\" \"   /");
            self::$console->println("                  (M^^.^~~:.'\" \").");
            self::$console->println("            -   (/  .    . . \\ \\)  -");
            self::$console->println("               ((| :. ~ ^  :. .|))");
            self::$console->println("            -   (\\- |  \\ /  |  /)  -");
            self::$console->println("                 -\\  \\     /  /-");
            self::$console->println("                   \\  \\   /  /");

            self::showSunkShips(self::$myFleet);
                
            if(GameController::checkIsFleetSunk(self::$myFleet)) {
                self::$console->setForegroundColor(Color::RED);
                self::$console->println("You lost!");
                self::$console->printAnimationLost();
                exit();
            }

        }
        self::showLeftoverShips(self::$myFleet);
    }
}
